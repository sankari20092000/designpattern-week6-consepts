package com.san.designpattern;

public class RedShapeDecorator extends ShapeDecorator {

	   public RedShapeDecorator(Shape d1) {
	      super(d1);		
	   }

	   @Override
	   public void draw() {
	      decoratedShape.draw();	       
	      setRedBorder(decoratedShape);
	   }

	   private void setRedBorder(Shape d1){
	      System.out.println("Border Color: Red");
	   }
	}
