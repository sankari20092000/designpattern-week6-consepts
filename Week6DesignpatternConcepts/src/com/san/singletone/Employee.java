package com.san.singletone;


import java.io.Serializable;

public class Employee implements Serializable{

	private static final long serialVersionId = 1L;//long use L
	private int eid;
	private String ename;
	public Employee()
	{
		
	}
	public int getEid() {
		return eid;
	}
	public Employee(int eid,String ename)
	{
		super();
		this.eid = eid;
		this.ename = ename;
	}
	
	public void setEid(int eid) {
		this.eid = eid;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	

}
