package com.san.singletone;

public class LazySingleton {
	private static volatile LazySingleton instance = null;
	 
    // private constructor
    private LazySingleton() {
    }
 
    public static LazySingleton getInstance() {
        if (instance == null) {
            synchronized (LazySingleton.class) {
                instance = new LazySingleton();
            }
        }
        return instance;
    }


}
