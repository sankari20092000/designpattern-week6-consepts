package com.san.singletone;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerializeableDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee emp=new Employee();
		emp.setEid(102);
		emp.setEname("san");
		

		try {
			//serialization
			FileOutputStream foo=new FileOutputStream("employee.ser");

			ObjectOutputStream oos= new ObjectOutputStream(foo);
	          oos.writeObject(emp);
		    //FileWriter w=new FileWriter("employee.ser")
	          System.out.println("Employee object created and serialized");
	          
	          //de-serialization
	          FileInputStream fis=new FileInputStream("employee.txt");	
	          ObjectInputStream ois=new ObjectInputStream(fis);
	          
	         Object e=(Employee) ois.readObject();
	     System.out.println("Employee Object De-serialization");   
	         System.out.println(emp.getEid()+" "+ emp.getEname());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	

	}

}
