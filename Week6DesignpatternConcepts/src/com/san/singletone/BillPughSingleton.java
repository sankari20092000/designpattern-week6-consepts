package com.san.singletone;

public class BillPughSingleton {
	   private BillPughSingleton() {
	    }
	 
	    private static class LazyHolder {//inner  class
	        private static final BillPughSingleton INSTANCE = new BillPughSingleton();
	    }
	 
	    public static BillPughSingleton getInstance() {
	        return LazyHolder.INSTANCE;
	    }

}
