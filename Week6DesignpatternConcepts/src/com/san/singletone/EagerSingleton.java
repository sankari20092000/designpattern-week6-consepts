package com.san.singletone;

public class EagerSingleton {//1.singleton with eager
	
	private static EagerSingleton instance=new EagerSingleton();
	
	private EagerSingleton()//private con use this class only
	{
		
		
	}
	public static EagerSingleton getInstance()
	{
		return instance;
	}

}
