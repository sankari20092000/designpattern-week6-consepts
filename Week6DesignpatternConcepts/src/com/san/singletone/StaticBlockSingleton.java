package com.san.singletone;

public class StaticBlockSingleton {
	private static final StaticBlockSingleton INSTANCE;
	//INSTANCE=null;//final var can't initialize
	static {
		try {
			INSTANCE =new StaticBlockSingleton();
			
		}catch(Exception e)
		{
			throw new RuntimeException("i was not excepting this!!!",e);
		}
	}
	
	private StaticBlockSingleton() {
		
	}
	
	public static StaticBlockSingleton getInstance()
	{
		return INSTANCE;
	}

}
