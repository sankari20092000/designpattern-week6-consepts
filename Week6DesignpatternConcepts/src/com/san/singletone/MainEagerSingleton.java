package com.san.singletone;

public class MainEagerSingleton {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(EagerSingleton.getInstance());//diff obj obj1
		System.out.println(EagerSingleton.getInstance());//diff object obj2
	
		System.out.println(LazySingleton.getInstance());
		System.out.println(LazySingleton.getInstance());

		System.out.println(StaticBlockSingleton.getInstance());//same obj
		System.out.println(StaticBlockSingleton.getInstance());
		
		System.out.println(BillPughSingleton.getInstance());
		System.out.println(BillPughSingleton.getInstance());
	

	}

}
