package com.san.exceptionhandling;

public class ExceptionHandling {

	public static void main(String[] args) {

		ExceptionHandling demo=new ExceptionHandling();

		try {
			System.out.println(demo.div(30,0));
			
			try {

			} catch (Exception e) {
				System.err.println("Exception occured");

			} finally {

			}
		} 
		catch (ArithmeticException ae) {
			try {

			} catch (Exception e1) {
				System.out.println("Can't div number  ");

			} finally {

			}

		}
		catch(NullPointerException e)
		{
			e.printStackTrace();
		}
		catch(Exception e)
		{
			e.getMessage();
		}
		finally {
			try {
				
			}catch(Exception e)
			{
				
			}
			finally {
				
			}
		}

	}
	public int div(int a,int b)//throws ArithmeticException
	{
		int res=0;
		try {
			res=a/b;
		}
		catch(ArithmeticException e)
		{
			System.err.println("Div module handle zero div by exception");
		}
		return res;

	}
	
	public int findLength(String str)
	{
		int len=0;
		try {
			len=str.length();
		}
		catch(NullPointerException | ArithmeticException e)
		{
			e.printStackTrace();
		}
		return len;
	}

}
