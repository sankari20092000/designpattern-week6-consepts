package com.san.factory;

 
public enum CarType {
    SMALL, SEDAN, LUXURY
}