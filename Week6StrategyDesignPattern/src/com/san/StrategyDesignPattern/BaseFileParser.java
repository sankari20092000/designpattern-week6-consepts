package com.san.StrategyDesignPattern;

public abstract class BaseFileParser{
	  public abstract void parseFile();
	  
	}