package com.san.inputoutputs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InputOutput {

	public static void main(String[] args) {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.println("Enter name");
			String s = br.readLine();
			System.out.println("Enter amount");//"9000"
			String str = br.readLine();

			int amt=Integer.parseInt(str);//parsexxx();
			System.out.println("Welcome :  "+s);
			System.out.println("Amount : "+amt);
			
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

}
