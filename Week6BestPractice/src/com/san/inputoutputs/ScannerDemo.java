package com.san.inputoutputs;

import java.util.Scanner;

public class ScannerDemo {

	public static void main(String[] args) {

		Scanner scan=new Scanner(System.in);
		
		System.out.println("Enter eid :");
	   int eid=scan.nextInt();//enter key
	   
	   scan.nextLine();
	   
	   System.out.println("Enter Name :");
	   String s=scan.nextLine();
	   
	   System.out.println("Eid : "+eid+"  Ename : "+s);
	
	   scan.close();
	}

}
