package com.san.protype;

public interface PrototypeCapable extends Cloneable {

    public PrototypeCapable clone() throws CloneNotSupportedException;

    
}
