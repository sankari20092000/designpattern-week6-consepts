package com.san.facadeDesignPattern;

public class FDIssuer {
	public void issueFD(Double amount) {
		System.out.println("FD for total amount "+amount+" issuesed");

	}

}
