package com.san.facadeDesignPattern;

public class AccountDebitor {
	 public void debitAccount(String accNo, Double amount)
	 {
          System.out.println("Amount "+amount+" Debited to the Account");
	 } 

}
