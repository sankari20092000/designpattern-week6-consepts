package com.san.facadeDesignPattern;

public class BankingFacade {
	 AccountCreditor creditor=new AccountCreditor();
	  AccountDebitor debitor=new AccountDebitor();
	  FDIssuer fdIssuer=new FDIssuer();
	  public void onlineTransfer(String fromAcc, String toAcc, Double amount){
	    System.out.println("online transfer");
		  debitor.debitAccount(fromAcc, amount);
	    creditor.creditAccount(toAcc, amount);
	  }
	  public void atmWithdrawal(String fromAcc, Double amount){
		   System.out.println("ATM withdrawl");
		  debitor.debitAccount(fromAcc, amount);
	  } 
	  public void issueFD(String fromAcc, Double amount){
		  debitor.debitAccount(fromAcc, amount);
	   System.out.println("FD issuesed");
	    fdIssuer.issueFD(amount);
	  } 

}
