package com.san.itreatorpattern;

public interface SequenceIterator<T> {
	public abstract T getCurrentItem();
	 public boolean isOver();
	 public void nextItem();
	 public void firstItem();

}
