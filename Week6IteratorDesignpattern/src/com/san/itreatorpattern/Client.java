package com.san.itreatorpattern;

import java.util.Arrays;
public class Client {
 public static void main(String[] args) {
	 
  //Iterating over a list sequence
  ListSequence<Integer> listSequence = new ListSequence<Integer>();
  listSequence.setList(Arrays.asList(12,20,28,36));
  ListSequenceIterator<Integer> listSeqIterator=  (ListSequenceIterator<Integer>)listSequence.createSeqIterator();
  System.out.println("Printing listSequence's items");
  printIteratorElements(listSeqIterator);
  
  
  //Iterating over a list sequence
  LinkedListSequence<Integer> linkedListSequence = new LinkedListSequence<Integer>();
  linkedListSequence.setList(Arrays.asList(111,122,108,242));
  LinkedListSequenceIterator<Integer> linkedListSeqIterator=(LinkedListSequenceIterator<Integer>)linkedListSequence.createSeqIterator();
  System.out.println("Printing linkedListSequence's items");
  printIteratorElements(linkedListSeqIterator);
 }
 
 private static void printIteratorElements(SequenceIterator<?> listSeqIterator){
  while(listSeqIterator.isOver()==false){
   System.out.println("item->"+listSeqIterator.getCurrentItem());
   listSeqIterator.nextItem();
  }
  
 }
}