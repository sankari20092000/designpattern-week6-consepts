package com.san.itreatorpattern;

public interface Sequence<T> {
	 public SequenceIterator<T> createSeqIterator();
	}
