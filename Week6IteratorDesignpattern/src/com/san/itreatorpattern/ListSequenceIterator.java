package com.san.itreatorpattern;
public class ListSequenceIterator<T> implements SequenceIterator<T>{
	 private ListSequence<T> list=null;
	 public ListSequenceIterator(ListSequence<T> list) {
	  super();
	  this.list = list;
	 }
	 private int currentItemNo=0;
	 @Override
	 public T getCurrentItem() {
	  return (this.list.getList().get(currentItemNo));
	 }
	 @Override
	 public boolean isOver() {
	  if(currentItemNo==list.getList().size()){
	   return true;
	  }else{
	   return false;
	  }
	 }
	 @Override
	 public void nextItem() {
	  this.currentItemNo++;
	 }
	 @Override
	 public void firstItem() {
	  this.currentItemNo=0;
	 }
	}


