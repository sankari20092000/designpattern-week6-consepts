package com.san.itreatorpattern;
import java.util.ArrayList;
import java.util.List;
public class LinkedListSequence<T> implements Sequence<T> {
 private List<T> list=new ArrayList<T>();
 @Override
 public LinkedListSequenceIterator<T> createSeqIterator() {
  return new LinkedListSequenceIterator<T>(this);
 }
public List<T> getList() {
	return list;
}
public void setList(List<T> list) {
	this.list = list;
}

}
