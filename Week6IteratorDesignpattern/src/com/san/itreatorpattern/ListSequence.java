package com.san.itreatorpattern;

import java.util.ArrayList;
import java.util.List;
public class ListSequence<T> implements Sequence<T> {
 private List<T> list=new ArrayList<T>();
 @Override
 public SequenceIterator<T> createSeqIterator() {
  return new ListSequenceIterator<T>(this);
 }
 //setter & getter for private list variable
public List<T> getList() {
	return list;
}
public void setList(List<T> list) {
	this.list = list;
}
}